%.pdf:
	pandoc \
		introduction.md \
	  01.md 02.md 03.md 04.md \
		conclusion.md preamble.yaml \
		--include-before-body titlepage.tex \
		--filter pandoc-fignos \
		--filter pandoc-citeproc \
		--csl gost2008.csl \
		--pdf-engine xelatex \
		--verbose \
	  --output $*.pdf

all: report.pdf

.PHONY: all

clean:
	rm -rf *.pdf
